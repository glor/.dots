#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <signal.h>

#include "config.h"

int error(const char err[]) {
	printf("Error: %s", err);
}
int getMax(void) {
	int max;
	FILE* fmax = fopen(BNESS_MAX,"r");
	if(fmax<0) {
		error("Can't read max Brightness");
		exit(1);
	}
	fscanf(fmax,"%d",&max);
	fclose(fmax);
	return max;
}

int getBrightness(void) {
	int max;
	FILE* fmax = fopen(BNESS,"r");
	if(fmax<0) {
		error("Can't read max Brightness");
		exit(1);
	}
	fscanf(fmax,"%d",&max);
	fclose(fmax);
	return max;
}

static volatile sig_atomic_t sflag;
static sigset_t signal_neu, signal_alt, signal_leer;

static void sigfunc(int sig_nr)
{
  fprintf(stdout, "SIGPIPE erhalten. Programmabbruch...\n");
  exit(0);
}

void signal_pipe(void) {
	if(signal(SIGPIPE, sigfunc) == SIG_ERR) {
		fprintf(stderr, "Konnte Signal SIGPIPE nicht erzeugen..........\n");
		exit(0);
    }
/*Wir entfernen alle Signale aus der Signalmenge*/
  sigemptyset(&signal_leer);
  sigemptyset(&signal_neu);
  sigaddset(&signal_neu, SIGPIPE);
/*Jetzt setzen wir signal_neu und sichern die*/
/* noch aktuelle Signalmaske in signal_alt*/
  if(sigprocmask(SIG_UNBLOCK, &signal_neu, &signal_alt) < 0)
    exit(0);
}

int checkServer(void) {	//returns -1==noFIFOFILE oder the PID of the file-locker of FIFOFILE
	if( access( FIFOFILE, F_OK ) == -1 ) {	//check if fifo exists
		return -1;
	}
	int bfifo;
	struct flock fl;
	fl.l_type   = F_RDLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
	fl.l_start  = 0;        /* Offset from l_whence         */
	fl.l_len    = 0;        /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */
	int pid = fork();
	printf("%d\n", pid);
	if(pid==-1) {
		error("cannot fork()");
		exit(1);
	}
	if(pid==0) {
		printf("c=%d\n",getpid());
		client((int)(0.5+((float)getBrightness()*100)/getMax()));
		exit(0);
	}
	printf("p=%d\n",getpid());
	bfifo = open(FIFOFILE, O_RDONLY);
	fcntl(bfifo, F_GETLK, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
	if(fl.l_type == F_UNLCK) {
		read(bfifo, &pid, sizeof(pid));//let fork exit correctly, if there is no real Server
	}
	close(bfifo);
	return fl.l_pid;
	
	
}

int server(void) {
	int c = checkServer();
	if(c!=getpid()&&c!=-1) {
		printf("Server already running on PID %d\n", c);
		return 1;
	}
	//unlink(FIFOFILE);
	int value;
	FILE* fbrightness;
	int bfifo;
	int max = getMax();
	
	struct flock fl;
	fl.l_type   = F_RDLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
	fl.l_start  = 0;        /* Offset from l_whence         */
	fl.l_len    = 0;        /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */

	//mknod("/backlight", S_IFIFO | 0xFF, 0);
	umask(0000);
	mkfifo(FIFOFILE, 0666);
	
	
	while(1) {
		fl.l_type   = F_RDLCK;  /* tell it to unlock the region */
		fcntl(bfifo, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
		bfifo = open(FIFOFILE, O_RDONLY);
		if(read(bfifo, &value, sizeof(value)) == -1) {
			error("While reading");
			exit(1);
		}
		printf("received %d\n", value);
		fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
		fcntl(bfifo, F_SETLK, &fl); /* set the region to unlocked */
		close(bfifo);
		if(value == -1) {
			printf("Server killed.");
			exit(0);
		}
		
		value = (max*value)/100;
		fbrightness = fopen(BNESS,"w");
		if(fbrightness == -1) {
			error("cannot change Brightness!");
			exit(1);
		}
		fl.l_type   = F_WRLCK;  /* tell it to unlock the region */
		fcntl(fbrightness, F_SETLKW, &fl);  /* F_GETLK, F_SETLK, F_SETLKW */
		if(fprintf(fbrightness, "%d", value)<0) {
			error("cannot change Brightness!");
			exit(1);
		}
		fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
		fcntl(fbrightness, F_SETLK, &fl); /* set the region to unlocked */
		fclose(fbrightness);
	}
	return 0;
}

int main(int argc, char **argv) {
	signal_pipe();
	if(argc > 3) {
		printf("Usage: %s n\%\n",argv[0]);
		return 1;
	}
	if(argc == 1) {
		printf("%d\%\n",(int)(0.5+((float)getBrightness()*100)/getMax()));	//add 0.5 for round
		return 0;
	}
	//printf("%s => %d\n",argv[1], strcmp(argv[1],"--format"));
	if(strcmp(argv[1],"--format")==0 || strcmp(argv[1],"-f")==0) {
		char format[] = "%";
		if(argc != 2) {	//no explicit format given
			strcat(format,argv[2]);
		}
		strcat(format,"f\n");
		printf(format,(((float)getBrightness()*100)/getMax()));	//add 0.5 for round
		return 0;
	}
	if(strcmp(argv[1],"-d") == 0 || strcmp(argv[1],"-D")==0 || strcmp(argv[1],"--Daemonize")==0) {
		server();
		return 0;
	}
	if(strcmp(argv[1],"-k") == 0|| strcmp(argv[1],"--kill")==0) {
		client(-1);
		return 0;
	}
	if(strcmp(argv[1],"-c") == 0) {
		int c = checkServer();
		if(c == -1) {
			printf("FIFO-File (%s) doesn't exist", FIFOFILE);
			return 0;
		}
		if(c == getpid()) {
			printf("No Server running.");
			return 0;
		}
		printf("Server running with PID %d", c);
		return 0;
	}
	/*
	int c = checkServer();
	if(c==-1 || c==getpid()) {
		printf("No server running.\n");
		return 1;
	}*/
	int value = atoi(argv[1]);
	if(value>100) {
		printf("Usage: %s n\%\n",argv[0]);
		return 1;
	}
	client(value);
	printf("brightness set to %d\%\n",value);
}
int client(int value) {
	int bfifo = open(FIFOFILE, O_WRONLY);
	struct flock fl;
	fl.l_type   = F_WRLCK;  /* F_RDLCK, F_WRLCK, F_UNLCK    */
	fl.l_whence = SEEK_SET; /* SEEK_SET, SEEK_CUR, SEEK_END */
	fl.l_start  = 0;        /* Offset from l_whence         */
	fl.l_len    = 0;        /* length, 0 = to EOF           */
	fl.l_pid    = getpid(); /* our PID                      */
	
	fcntl(bfifo, F_SETLKW, &fl);  /* set the lock, waiting if necessary */
	
    if (write(bfifo, &value, sizeof(value)) == -1) {
        error("while writing to Server");
    }
    close(bfifo);
    fl.l_type   = F_UNLCK;  /* tell it to unlock the region */
	fcntl(bfifo, F_SETLK, &fl); /* set the region to unlocked */
    return 0;
}
