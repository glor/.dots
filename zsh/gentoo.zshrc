# copy this file to ~/.zshrc

#zstyle :compinstall filename '/home/glor/.zshrc'

source ~/.dots/zsh/zshrc

# add local options here
# gentoo here
export BROWSER='firefox-bin'
alias smnt='sudo mount -o uid=1000'
#alias noice='~/bin/bin/noice'
alias ba='~/bin/bin/backlight'
alias poweroff='gksudo systemctl poweroff'
alias reboot='gksudo systemctl reboot'
alias mount='mount -o uid=1000'
alias ssh='TERM=linux ssh'
